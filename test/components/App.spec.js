import React from 'react';
import App from '../../client/components/App'

describe('App', () => {
  const wrapper = mount(<App />);

  it('renders without exploding', () => {
    expect(wrapper).to.have.length(1);
  });

});