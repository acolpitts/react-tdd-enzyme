import React from 'react';
import Comment from '../../client/components/Comment';
import CommentList from '../../client/components/CommentList';

describe('Comment', () => {
  const wrapper = shallow(<Comment />);

  it('should be a list item', () => {
    expect(wrapper.type()).to.eql('li');
  });

  it('renders the custom comment text', () => {
    wrapper.setProps({ comment: 'sympathetic ink' });
    expect(wrapper.contains('sympathetic ink')).to.equal(true);
  });

  it('has a class name of "comment-item"', () => {
    expect(wrapper.find('.comment-item')).to.have.length(1);
  })

  it('calls componentDidMount', () => {
    spy(CommentList.prototype, 'componentDidMount');

    const wrapper = mount(<CommentList />);
    expect(CommentList.prototype.componentDidMount.calledOnce).to.equal(true);
  });

});