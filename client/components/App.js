import React, { Component } from 'react';
import CommentList from "./CommentList";

export default class App extends Component {
  render() {
    return (
      <section className='App'>
        <CommentList></CommentList>
      </section>
    );
  }
}
